{
  description = "todoweb";
  inputs = {
    nixpkgs.url = github:nixos/nixpkgs/nixos-unstable;
    flake-utils.url = github:numtide/flake-utils;
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }: let
    utils = flake-utils.lib;
  in
  utils.eachDefaultSystem (system: let
    compilerVersion = "ghc902";
    pkgs = nixpkgs.legacyPackages.${system};
    hsPkgs = pkgs.haskell.packages.${compilerVersion}.override {
      overrides = hfinal: hprev: {
        todoweb = hfinal.callCabal2nix "todoweb" ./. {};
      };
    };
  in rec {
    packages =
      utils.flattenTree
      {todoweb = hsPkgs.todoweb;};

      devShell = hsPkgs.shellFor {
        withHoogle = true;
        packages = p: [
          p.todoweb
        ];
        buildInputs = [
          # pkgs.cabal2nix
          pkgs.cabal-install
          #          pkgs.postgresql
          pkgs.hlint
          pkgs.ormolu
          pkgs.elmPackages.elm
          pkgs.elmPackages.elm-format
          pkgs.haskell.compiler."${compilerVersion}"
#          pkgs.haskell.compiler.ghcjs # ghcjs
          hsPkgs.ghcid

          # pkgs.qemu
          # pkgs.purescript
          # hsPkgs.haskell-language-server
          # hsPkgs.alex
          # hsPkgs.happy
          # hsPkgs.hspec-discover
        ];
        # ++ (builtins.attrValues (import ./scripts.nix {s = pkgs.writeShellScriptBin;}));
      };

      defaultPackage = packages.todoweb;
  });
}
