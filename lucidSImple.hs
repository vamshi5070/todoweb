import Lucid

helloWorld :: Html ()
helloWorld =
  html_ $ do
    head_ $ title_ "Hello, World!"
    body_ $ do
      h1_ "Hello, World!"
      p_ "This is a simple example of how to use the Lucid library to create a web page"

main :: IO ()
main = putStrLn (renderText helloWorld)
