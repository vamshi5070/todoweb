{-# Language NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import Miso

main :: IO ()
main = startApp App {..}
  where
    model = "Hello, Miso!"
    update _ = noEff
    view = text
