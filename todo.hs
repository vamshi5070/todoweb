{-# LANGUAGE OverloadedStrings#-}

import Web.Scotty
import Data.Text.Lazy hiding (map)
import Data.Monoid (mconcat)
import Data.List

-- Define a list to hold our to-do items
todoList :: [String]
todoList = []

main :: IO ()
main = scotty 3000 $ do
    -- Display the current to-do list
    get "/" $ do
        html $ mconcat ["<h1>To-Do List</h1>", todoListHtml todoList]

    -- Add a new item to the to-do list
    post "/add" $ do
        item <- param "item"
        todoList `seq` (todoList `mappend` [item])
        html $ mconcat ["<h1>To-Do List</h1>", todoListHtml todoList]

    -- Remove an item from the to-do list
    post "/remove" $ do
        item <- param "item"
        todoList `seq` (todoList \\ [item])
        html $ mconcat ["<h1>To-Do List</h1>", todoListHtml todoList]

-- Helper function to convert the to-do list to HTML
todoListHtml :: [String] -> Text
todoListHtml items = mconcat ["<ul>", listItemsHtml items, "</ul>"]

-- Helper function to convert a list of items to HTML
listItemsHtml :: [String] -> Text
listItemsHtml = mconcat . map (pack . (\x -> "<li>" ++ x ++ "</li>"))
