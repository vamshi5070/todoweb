{-# LANGUAGE RecordWildCards            #-}


import Miso
import Miso.String (ms)
import Language.Javascript.JSaddle.Run as JSaddle

runApp :: JSM () -> IO ()
runApp = JSaddle.run 8080

-- Model
data Model = Model
  { tasks :: [String]
  } deriving (Eq, Show)

-- Initial model
initialModel :: Model
initialModel = Model []

-- Action type
data Action
  = AddTask String
  | RemoveTask Int

-- Update function
update :: Action -> Model -> Effect Action Model
update action model =
  case action of
    AddTask task ->
      noEff (Model (task : tasks model))
    RemoveTask index ->
      noEff (Model (removeTask index (tasks model)))

-- Remove task from list
removeTask :: Int -> [a] -> [a]
removeTask index xs = take index xs ++ drop (index + 1) xs

-- View function
view :: Model -> View Action
view model = div_ []
  [ h1_ [] [ text "Todo List" ]
  , input_ [ onInput AddTask ]
  , ul_ [] (map (\(task, index) -> li_ [ onClick (RemoveTask index) ] [ text (ms task) ]) (zip (tasks model) [0..]))
  ]

-- Start the app
main :: IO ()
main = runApp $ startApp App {..}
  where
    initialAction = AddTask "Initial Task"
    model = initialModel
    update = update
    view = view
    events = defaultEvents
    subs = []
