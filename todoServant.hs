{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

import Data.Aeson
import Data.Text
import GHC.Generics
import Servant
import Network.Wai.Handler.Warp (run)
--import Data.List

data Todo = Todo
  { task :: Text
  , done :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON Todo
instance FromJSON Todo

type API = "todo" :> (
         Get '[JSON] [Todo]
    :<|> ReqBody '[JSON] Todo :> Post '[JSON] Todo
    :<|> Capture "id" Int :> (
            Get '[JSON] Todo
        :<|> Delete '[JSON] ()
        :<|> ReqBody '[JSON] Todo :> Put '[JSON] Todo
     )
    )

todos :: [Todo]
todos = []

server :: Server API
server =  allTodos
    :<|> newTodo
    :<|> todoById
  where
    allTodos = return todos
    newTodo todo = do
        let newId = Prelude.length todos
        let newTodo = todo { task = task todo, done = done todo }
        return newTodo
    todoById id =
        case find (\t -> id == id) todos of
            Just todo -> (getTodo :<|> deleteTodo :<|> updateTodo) todo
            Nothing   -> throwError err404
      where
        getTodo todo = return todo
        deleteTodo _ = return ()
        updateTodo todo = return todo

api :: Proxy API
api = Proxy

main :: IO ()
main = run 8080 $ serve api server
