{-# LANGUAGE OverloadedStrings #-}
import Network.Wai
import Network.HTTP.Types
import Network.Wai.Handler.Warp (run)

app :: Application
app _req respond = respond $
    responseLBS status200 [("Content-Type", "text/plain")] "Hello, World!"

main :: IO ()
main = run 3000 app
