{-# LANGUAGE OverloadedStrings #-}

import Okapi
import Network.HTTP.Types
import Data.Text.Encoding (encodeUtf8)
import Network.Wai
import Network.Wai.Handler.Warp (run)


app :: Application
app req respond = case pathInfo req of
  ["hello"] -> respond $ responseLBS status200 [(hContentType, "text/plain")] (encodeUtf8 "Hello World!")
  _         -> respond $ responseLBS status404 [(hContentType, "text/plain")] (encodeUtf8 "404 Not Found")

main :: IO ()
main = run 3000 app
