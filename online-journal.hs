import Network.Wai
import Network.Wai.Handler.Warp
import Network.HTTP.Types
import Data.Time
import qualified Data.ByteString.Lazy as LBS

data JournalEntry = JournalEntry {
  entryContent :: String,
  entryTime :: UTCTime
}

journalEntries :: [JournalEntry]
journalEntries = ["rgv"]

app :: Application
app request respond = do
  currentTime <- getCurrentTime
  case requestMethod request of
    "POST" -> do
      body <- requestBody request
      let entry = JournalEntry (LBS.unpack body) currentTime
      let newJournalEntries = entry : journalEntries
      respond $ responseLBS status201 [(hContentType, "text/plain")] (LBS.pack $ show newJournalEntries)
    _ -> respond $ responseLBS status200 [(hContentType, "text/plain")] (LBS.pack $ show journalEntries)

main :: IO ()
main = run 8080 app
