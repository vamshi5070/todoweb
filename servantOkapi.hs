{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}
{-# LANGUAGE OverloadedStrings #-}

import Servant
import Servant.Server
import Okapi

type API = "hello" :> Get '[PlainText] String

server :: Server API
server = return "Hello World!"

app :: Application
app = serve (Proxy :: Proxy API) server

{-
main :: IO ()
main = run 3000  app
-}
