{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}
{-# LANGUAGE OverloadedStrings #-}

--import Okapi

import Servant.API

import Servant
import Servant.Server

import Network.Wai.Handler.Warp (run)

type API = "hello" :> Get '[PlainText] String

server :: Server API
server = return "Hello World!"

app :: Application
app = serve (Proxy :: Proxy API) server

main :: IO ()
main = run 8000 app
