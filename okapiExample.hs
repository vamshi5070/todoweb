{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

import Data.Text
import Okapi
import Okapi.Function

type Server = OkapiT IO

main :: IO ()
main = run greet

greet = do
  methodGET
  pathParam @Text `is` "greet"
  name <- pathParam
  pathEnd
  return $ setPlaintext ("Hello " <> name <> "! I'm Okapi.") $ ok