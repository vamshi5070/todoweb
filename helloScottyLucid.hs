{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty
import Lucid

main :: IO ()
main = scotty 3000 $ do
    get "/" $ do
        html $ renderText helloWorld
    get "/styles.css" $ do
        setHeader "Content-Type" "text/css"
        text (renderText (myStyles))

helloWorld :: Html ()
helloWorld = do
    link_ [rel_ "stylesheet", type_ "text/css", href_ "/styles.css"]
    h2_ [] (toHtml ("Hello, World!" :: String ))

myStyles :: Html ()
myStyles = do
    ".hello" ? do
        color: blue;
        textAlign: center;
        fontSize (px 32)