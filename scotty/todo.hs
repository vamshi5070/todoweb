{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty
import Data.Monoid (mconcat)
import qualified Data.Text.Lazy as LT

import Control.Concurrent.STM
import Control.Monad.IO.Class

import qualified Data.Text.Lazy.Builder as TB

import Text.Blaze.Html.Renderer.Text (renderHtml)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

--tasksTVar :: TVar [String]\
--tasksTVar :: IO (TVar [String])
main = do
    tasksTVar <- atomically $ newTVar ["Watch porn"]
    scotty 3000 $ do
        get "/" $ do
            tasks <- liftIO $ atomically $ readTVar tasksTVar
            html $ renderHtml $ do
                 tasksHTML tasks
                 newTaskForm
                 clearTaskForm
        post "/add" $ do
            newTask <- param "newTask"
            liftIO $ atomically $ do
                tasks <- readTVar tasksTVar
                writeTVar tasksTVar $ tasks <> [newTask]
            redirect "/"
        post "/clear" $ do
            liftIO $ atomically $ do
                writeTVar tasksTVar  []
            redirect "/"

{-
tasksHTML :: [String] -> String
tasksHTML tasks = mconcat $ map (\x -> "<li>" <> x <> "</li>") tasks
-}

tasksHTML :: [String] -> H.Html
tasksHTML tasks = do
    H.h1 "Todo List"
    H.ul $ mapM_ (H.li . H.toHtml) tasks

newTaskForm :: H.Html
newTaskForm = do
       H.form H.! A.method "post" H.! A.action "/add" $ do
          H.label "New Task: " H.! A.for "newTask"
          H.input H.! A.type_ "text" H.! A.id "newTask" H.!  A.name "newTask"
          H.button "Add task"

clearTaskForm :: H.Html
clearTaskForm = do
       H.form H.! A.method "post" H.! A.action "/clear" $ do
       H.button "Clear all tasks" --H.! A.name "clearTask"