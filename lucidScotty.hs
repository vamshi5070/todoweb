{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty
import Lucid
import Control.Monad.IO.Class (liftIO)
import Data.Text.Lazy (pack)

data Model = Model {tasks :: [String], newTask :: String}

initialModel :: Model
initialModel = Model [] ""

addTask :: Model -> Model
addTask (Model ts nt) = Model (nt : ts) ""

removeTask :: Int -> Model -> Model
removeTask i (Model ts _) = Model (removeTask' i ts) ""

removeTask' :: Int -> [a] -> [a]
removeTask' i xs = take i xs ++ drop (i + 1) xs

updateTask :: String -> Model -> Model
updateTask s (Model ts _) = Model ts s

view :: Model -> Html ()
view (Model tasks newTask) = do
    form_ [method_ "post"] $ do
        input_ [type_ "text", name_ "task", value_ (pack newTask)]
        input_ [type_ "submit", value_ "Add Task"]
    ul_ [] (map (\(task,index) -> li_ [] [toHtml task, form_ [method_ "post", action_ (pack ("/remove/" <> show index))] $ input_ [type_ "submit", value_ "Remove"]]) (zip tasks [0..]))

main :: IO ()
main = scotty 3000 $ do
        post "/" $ do
         task <- param "task"
         let model = updateTask task initialModel
         html (renderText (view model))
        post "/remove/:index" $ do
         index <- param "index"
         let model = removeTask index initialModel
         html (renderText (view model))
        get "/" $ do
         html (renderText (view initialModel))
