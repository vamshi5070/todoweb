import Lucid
import qualified Data.Text as T

-- Model
data Model = Model
  { tasks :: [String]
  , newTask :: String
  } deriving (Eq, Show)

-- Initial model
initialModel :: Model
initialModel = Model [] ""

-- Action type
data Action
  = AddTask
  | RemoveTask Int
  | UpdateTask String

-- Update function
update :: Action -> Model -> Model
update action model =
  case action of
    AddTask ->
      Model (newTask model : tasks model) ""
    RemoveTask index ->
      Model (removeTask index (tasks model)) (newTask model)
    UpdateTask task ->
      Model (tasks model) task

-- Remove task from list
removeTask :: Int -> [a] -> [a]
removeTask index xs = take index xs ++ drop (index + 1) xs

-- View function
view :: Model -> Html ()
view model = do
  form_ [onSubmit AddTask] $ do
    input_ [type_ "text", value_ (T.pack (newTask model)), onInput UpdateTask]
    input_ [type_ "submit", value_ "Add Task"]
  ul_ [] (map (\(task, index) ->
    li_ [] [toHtml task, button_ [onClick (RemoveTask index)] "Remove"]) (zip (tasks model) [0..]))

-- Start the app
main :: IO ()
main = runLucidApp initialModel update view
