{-# LANGUAGE OverloadedStrings #-}


import Okapi

main :: IO ()
main = okapi 3000 $ do
    get "/" $ text "Hello World!"

{-
import Data.Text
import Okapi

main :: IO ()
main = runOkapi id 3000 greet

greet = do
  seg "greet"
  name <- segParam
  respondPlainText [] $ "Hello " <> name <> "! I'm Okapi."

-}