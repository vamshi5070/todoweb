import Html exposing (..)
import Html.Events exposing (onClick)
import Http
import Json.Decode as Decode exposing (..)
import Json.Encode as Encode exposing (..)
import Browser

type alias Task =
    { task : String
    , completed : Bool
    , todoId : Int
    }

type alias Model =
    { tasks : List Task
    , newTask : String
    }

type alias Flags = { name : String
    , age : Int
    }
    
init : Flags -> (Model, Cmd Msg)
init _ =
    ({ tasks = []
    , newTask = ""
    },Cmd.none)

-- Define a Msg type to handle updates to the model
type Msg
    = TasksLoaded (Result Http.Error (List Task))
    | NewTaskInput String
    | TaskCreated (Result Http.Error Task)

-- Define an update function to handle Msgs
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TasksLoaded (Ok tasks) ->
            ( { model | tasks = tasks }, Cmd.none )

        TasksLoaded (Err _) ->
            ( model, Cmd.none )

        NewTaskInput value ->
            ( { model | newTask = value }, Cmd.none )

        TaskCreated (Ok task) ->
            ( { model | newTask = "", tasks = task :: model.tasks }, Cmd.none )

        TaskCreated (Err _) ->
            ( model, Cmd.none )

-- Define a function to send a GET request to the server to retrieve the list of tasks
getTasks : Cmd Msg
getTasks = Cmd.none--text "hello"
    --Http.send TasksLoaded (Http.get "http://localhost:8080/todos")

-- Define a function to send a POST request to the server to create a new task
{-
createTask : String -> Cmd Msg
createTask newTask =
    let
        request =
            { body = Encode.object [ ( "task", Encode.string newTask ), ( "completed", Encode.bool False ) ]
            , headers = [ ( "Content-Type", "application/json" ) ]
            , method = "POST"
            , url = "http://localhost:8080/todos"
            }
    in
    Http.send TaskCreated (Http.request request |> Http.fromJson Decode.value)
-}
-- Define a view function to display the list of tasks and a form to create new tasks
view : Model -> Html Msg
view model = text "Rgv"
    {-
    div []
        [ h1 [] [ text "Tasks" ]
        , ul [] (List.map taskView model.tasks)
        , form []
            [ input [ type_ "text", value model.newTask, onInput NewTaskInput ] []
            , button [ type_ "submit", onClick (createTask model.newTask) ] [ text "Add Task" ]
            ]
        ]
-}
taskView : Task -> Html msg
taskView task =
    li [] [ text task.task ]

subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none

-- Define the main function to start the application
main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions  =  subscriptions --Sub.none--\model -> getTasks
        , view = view
        }
