-- module Main exposing (..)

import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)
import Http
import Browser

type alias Model =
    { message : String
    }

-- type Msg
    -- = ButtonClicked
    -- | FetchFailed Http.Error
    -- | FetchSucceeded String

type Msg
  = GotText (Result Http.Error String)

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    -- case msg of
        -- ButtonClicked ->
            (model, fetchMessage)

        -- FetchFailed _ ->
            -- ({model | message = "Failed to fetch message"}, Cmd.none)

        -- FetchSucceeded message ->
            -- ({model | message = message}, Cmd.none)

fetchMessage : Cmd Msg
fetchMessage =
    Http.get  
        {
            url = "http://localhost:8080/hello"
        , expect = Http.expectString GotText --(Err  FetchFailed ) --FetchSucceeded
        }

-- first = 
        
view : Model -> Html Msg
view model =
    div []
        [
        -- [ button [ onClick ButtonClicked ] [ text "Fetch Message" ]
         div [] [ text model.message ]
        ]

main : Program () Model Msg
main =
    Browser.element
    --Html.program
        { init = (\_ -> ({ message = "Click the button to fetch the message" }, Cmd.none))
        , view = view
        , update = update
        , subscriptions = (\_ -> Sub.none)
        }
