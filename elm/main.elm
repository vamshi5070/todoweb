module Main exposing (main)

--import Browser
import Html exposing (Html,div,text,button)
import Html.Attributes exposing (style)
import Browser

main : Html msg
main = div 
         [
          style "background-color" "navy"
          ,style "color" "white"
          ,style "padding" "10px"
          ,style "font-size" "24px"
          , style "font-family" "Fira code"
         ]
         [text "rgv"

        , button [ ] [ text "Try Again!" ]
         ]
