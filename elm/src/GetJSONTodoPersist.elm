module GetJSONTodoPersist exposing (..)

import Browser
import Browser.Events exposing (onKeyDown)
import Html exposing (..)
import Html.Attributes exposing (height,class,name,width,style, value, placeholder,autofocus)
import Html.Events exposing (..)
import Http
-- import Css exposing (..)
import Json.Decode exposing (Decoder,list,map3, map4, field, int, string, bool)
import Json.Encode --exposing (encode,string)
import Json.Decode as Json --exposing (encode,string)
import Html.Lazy exposing (lazy,lazy2)

-- MAIN

main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view
    }

-- MODEL
type GetModel
  = Failure
  | Loading
  | Success (List Todo)
  -- | Add String
  -- | Task String

type alias Model = {
        pull : GetModel
        ,push : String
            }

-- type PostModel = String

type alias Todo =
  { taskWithId : String
  , completedWithId : Bool
  , todoId : Int
  }

init : () -> (Model, Cmd Msg)
init _ =
  ({ pull = Loading,push = ""}, getRandomQuote)

type Msg
  = MorePlease
  | GotTodo (Result Http.Error (List Todo))
  | UpdateField String
  | Add
  | Flip Int

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    MorePlease ->
      ({model | pull = Loading}, getRandomQuote)
    GotTodo result ->
      case result of
        Ok todos -> 
          ({model | pull = Success todos}, Cmd.none)
        Err _ ->
          ({model | pull=Failure}, Cmd.none)

    UpdateField task ->  ({model | push = task},Cmd.none)
              
    Add ->  ({ pull = Loading, push = ""} ,   postTask  ("\"" ++ model.push ++ "\"")     )

    Flip tId -> (model,upload tId)

-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none

-- VIEW
view : Model -> Html Msg
view model =
     div [] [
         blockquote [
              --style "background-color" "#000000"
             --,style "color" "white"
             style "padding" "10px"
             ,style "font-size" "24px"
             , style "font-family" "fira code"
             ]
             [viewInput model.push]
             {-
              [h1 [style "text-align" "center"] [ input [placeholder "add todo",width  400
                                                         ,height 200,autofocus  True,onEnter Add
                                                             --(\x -> GotTask (Ok x))
                                                         , onInput UpdateField] []]
              --input [placeholder "add todo", value "rgv", onInput (\x -> "") ] ]
                   -- 
             ]
             -}
             ,h3 [
              style "text-align" "left"
             ,style "font-size" "24px"
             , style "font-family" "fira code"
              ] [text "Incomplete:- "]

   ,       blockquote [style "background-color" "red"
             ,style "color" "white"
             ,style "padding" "10px"
             ,style "font-size" "24px"
             , style "font-family" "fira code"
             ]
              [lazy viewTodoIncomplete model]
              ,h3 [
              style "text-align" "left"
             ,style "font-size" "24px"
             , style "font-family" "fira code"
              ] [text "Completed:- "]
         , blockquote [style "background-color" "green"
             ,style "color" "white"
             ,style "padding" "10px"
             ,style "font-size" "24px"
             , style "font-family" "fira code"
             ]
              [lazy viewTodoCompleted model]
             ]

viewTodoCompleted : Model -> Html Msg
viewTodoCompleted model =
  case model.pull of
    Failure ->
      div []
        [ text "I could not load a task for some random reason. "
        , button [ onClick MorePlease ] [ text "Try Again!" ]
        ]
    Loading ->
      text "Loading..."

    Success todos ->
      div []
        [
         blockquote [] <| List.map (\x -> myFilter x True ) todos 
        ]

viewTodoIncomplete :  Model -> Html Msg
viewTodoIncomplete model = 
   case model.pull of
    Failure ->
      div []
        [ text "I could not load a task for some random  reason."
        , button [ onClick MorePlease ] [ text "Try Again!" ]
        ]
    Loading ->
      text "Loading..."

    Success todos ->
      div []
        [
         blockquote [] <| List.map (\x -> myFilter x False) todos 
        ]

         
myFilter : Todo -> Bool -> Html Msg
myFilter myTodo pred = 
           case myTodo.completedWithId ==  pred of
               True ->  section [] [input  [value myTodo.taskWithId] [], button [ onClick (Flip myTodo.todoId) ] [text (if pred then "Not Done" else "Done")]]
               False ->  p [] []

-- HTTP

getRandomQuote : Cmd Msg
getRandomQuote =
  Http.get
    { url = "http://localhost:8081/todos"
    , expect = Http.expectJson GotTodo  (list quoteDecoder)
    }

quoteDecoder : Decoder Todo
quoteDecoder  = 
   map3 Todo
    (field "taskWithId" string)
    (field "completedWithId" bool)
    (field "todoId" int) 

postTask : String -> Cmd Msg
postTask task =
  Http.post
    { url = "http://localhost:8081/todos"
    , body = Http.stringBody "application/json" task
    , expect = Http.expectJson GotTodo  (list quoteDecoder)
--GotTask string
               -- (Json.decodeString string task)--(Json.succeed ())--GotTask string
    }       

upload : Int -> Cmd Msg
upload tId =
  Http.request
    { method = "PATCH"
    , headers = []
    , url = "http://localhost:8081/todos"--"https://example.com/publish"
    , body = Http.jsonBody (Json.Encode.int tId)
    , expect = Http.expectJson GotTodo  (list quoteDecoder)
    -- , expect = Http.expectWhatever Uploaded
    , timeout = Nothing
    , tracker = Nothing
    }
        
onEnter : Msg -> Attribute Msg
onEnter msg =
    let
        isEnter code =
            if code == 13 then
                Json.succeed msg
            else
                Json.fail "not ENTER"
    in
        on "keydown" (Json.andThen isEnter keyCode)

viewInput : String -> Html Msg
viewInput task = --button [] [text "Rgv"]
    header
        [ class "header" ]
        [ h1 [] [ text "todos" ]
        , input
            [ class "new-todo"
            , placeholder "What needs to be done?"
            , autofocus True
            , value task
            , name "newTodo"
            , onInput UpdateField
            , onEnter  Add--(GotTask (Ok  (task) ))
            --Add
            ] []
            -- , button [] [text "Rgv"]
            ,button [ onClick (Add) ,style "color" "black",style "background-color" "white"]  [text "Submit"]
        ]
